#include <iostream>


class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "noise \n";
	};

private:

};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof \n";
	};

private:

};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Mrrr \n";
	};

private:

};

class Mouse : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Peepeepee \n";
	};

private:

};


int main() {
	Animal* arr[3];
	arr[0] = new Dog;
	arr[1] = new Cat;
	arr[2] = new Mouse;

	for (int i = 0; i < 3; i++)
	{
		arr[i]->Voice();
		delete arr[i];
	}

}